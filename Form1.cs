﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace IndTools
{   
    public partial class formIndicador : Form
    {
        public formIndicador()
        {
            InitializeComponent();
        }
        

        public string LoadCandles(List<string> _time, List<double> _open, List<double> _high, List<double> _low, List<double> _close)
        {
            try
            {
                string nome = "Preços";
                Series serie = this.cGraficos.Series.FirstOrDefault(x => x.Name == nome);
                if (serie == null) serie = this.cGraficos.Series.Add(nome);
                serie.ChartType = SeriesChartType.Candlestick;
                serie.Points.Clear();                                
                int index = 0;
                while (index < _open.Count())
                {
                    serie.Points.AddXY(_time[index],_high[index],_low[index],_open[index],_close[index]);
                    index++;
                }
                this.cGraficos.Update();
                this.Refresh();
                return "OHLC Drawn";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            
        }

        public string LoadValores(List<string> X, List<double> valores, SeriesChartType tipoGrafico, string nome, KnownColor cor = KnownColor.Black, int size = 10)
        {
            try
            {
                Series serie = this.cGraficos.Series.FirstOrDefault(x => x.Name == nome);
                if (serie == null) serie = this.cGraficos.Series.Add(nome);
                serie.ChartType = tipoGrafico;
                serie.Points.Clear();
                serie.Color = Color.FromKnownColor(cor);
                serie.EmptyPointStyle.IsValueShownAsLabel = false;
                serie.EmptyPointStyle.MarkerSize = 0;
                int index = 0;
                foreach (double val in valores) { serie.Points.AddXY(X[index], val); index++; }
                this.cGraficos.Update();
                this.Refresh();
                return "Load: "+nome+" completed!";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            foreach (Series serie in cGraficos.Series)
            {
                int i = 0;
                while (i < serie.Points.Count() - (int)barrasUpDown.Value)
                {
                    DataPoint first = serie.Points.FirstOrDefault();
                    if (first != null) serie.Points.Remove(first);
                    i++;
                }
            }
        }

        private void formIndicador_Load(object sender, EventArgs e)
        {
            this.tabControl.Enabled = false;
        }
    }
}
