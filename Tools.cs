﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;


namespace IndTools
{
    public class CSharpTools
    {
        public static List<double> _open = new List<double>();
        public static List<double> _high = new List<double>();
        public static List<double> _low = new List<double>();
        public static List<double> _close = new List<double>();
        public static List<string> _time = new List<string>();
        public static List<double> topos = new List<double>();
        public static List<double> vales = new List<double>();

        private static formIndicador formInd;

        public static string msgBack = "";

        public static string GetMessage() { string temp = msgBack; msgBack = ""; return temp; }

        public static void LoadPrecos(string[] timeArr, double[] openArr, double[] highArr, double[] lowArr, double[] closeArr)
        {
            _open.Clear(); _high.Clear(); _low.Clear(); _close.Clear();
            _open.AddRange(openArr.Reverse().ToList<double>());
            _high.AddRange(highArr.Reverse().ToList<double>());
            _low.AddRange(lowArr.Reverse().ToList<double>());
            _close.AddRange(closeArr.Reverse().ToList<double>());
            _time.AddRange(timeArr.Reverse().ToList<string>());
            if (formInd != null) formInd.LoadCandles(_time, _open, _high, _low, _close);
        }

        public static int LoadTopos(double[] arr, uint tipoGrafico = (uint)SeriesChartType.Column)
        {
            topos.Clear();
            lock (arr) topos.AddRange(arr.Reverse().ToList<double>());
            if (formInd == null) return topos.Count();
            if (topos.Count == 0) return 0;
            lock (topos) msgBack = formInd.LoadValores(_time, topos, (SeriesChartType)tipoGrafico, "Topos", System.Drawing.KnownColor.Blue);
            return topos.Count();
        }

        public static int LoadVales(double[] arr, uint tipoGrafico = (uint)SeriesChartType.Column)
        {
            vales.Clear();
            lock (arr) vales.AddRange(arr.Reverse().ToList<double>());
            if (formInd == null) return vales.Count();
            if (vales.Count == 0) return 0;
            lock (vales) msgBack = formInd.LoadValores(_time, vales, (SeriesChartType)tipoGrafico, "Vales", System.Drawing.KnownColor.Red);
            return vales.Count();
        }

        public static void ShowForm(string formName)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            Type[] types = assembly.GetTypes();
            foreach (Type type in types)
            {
                if (type.BaseType == typeof(Form) && type.Name == formName)
                {
                    formInd = type.Assembly.CreateInstance(type.FullName) as formIndicador;
                    Thread thread = new Thread(() => Application.Run(formInd));
                    thread.SetApartmentState(ApartmentState.STA);
                    thread.Start();
                    Thread.Sleep(200);
                    formInd.cGraficos.ChartAreas[0].AxisX.ScaleView.Zoomable = true;
                    formInd.cGraficos.ChartAreas[0].AxisY.ScaleView.Zoomable = true;
                    formInd.cGraficos.ChartAreas[0].CursorX.AutoScroll = true;
                    formInd.cGraficos.ChartAreas[0].CursorY.AutoScroll = true;
                    formInd.cGraficos.ChartAreas[0].CursorX.IsUserSelectionEnabled = true;
                    formInd.cGraficos.ChartAreas[0].CursorY.IsUserSelectionEnabled = true;
                }
            }
        }

        public static void EnableGraphs()
        {
            try
            {
                if (formInd != null) formInd.tabControl.Enabled = true;
                msgBack = "Series count:" + formInd.cGraficos.Series.Count() + " -> Tab Control Enabled!";
            }
            catch (Exception ex)
            {
                msgBack = ex.Message;
            }
        }

    }
}
