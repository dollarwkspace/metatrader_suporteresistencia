﻿
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
namespace IndTools
{
    partial class formIndicador
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabSummary = new System.Windows.Forms.TabPage();
            this.tabGraph = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.barrasUpDown = new System.Windows.Forms.NumericUpDown();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cGraficos = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabHisto = new System.Windows.Forms.TabPage();
            this.cHistograma = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabControl.SuspendLayout();
            this.tabGraph.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.barrasUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cGraficos)).BeginInit();
            this.tabHisto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cHistograma)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabSummary);
            this.tabControl.Controls.Add(this.tabGraph);
            this.tabControl.Controls.Add(this.tabHisto);
            this.tabControl.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(1068, 564);
            this.tabControl.TabIndex = 0;
            // 
            // tabSummary
            // 
            this.tabSummary.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tabSummary.Location = new System.Drawing.Point(4, 22);
            this.tabSummary.Name = "tabSummary";
            this.tabSummary.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tabSummary.Size = new System.Drawing.Size(1060, 538);
            this.tabSummary.TabIndex = 2;
            this.tabSummary.Text = "Controle";
            this.tabSummary.UseVisualStyleBackColor = true;
            // 
            // tabGraph
            // 
            this.tabGraph.Controls.Add(this.groupBox1);
            this.tabGraph.Controls.Add(this.cGraficos);
            this.tabGraph.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.tabGraph.Location = new System.Drawing.Point(4, 22);
            this.tabGraph.Name = "tabGraph";
            this.tabGraph.Padding = new System.Windows.Forms.Padding(3);
            this.tabGraph.Size = new System.Drawing.Size(1060, 538);
            this.tabGraph.TabIndex = 0;
            this.tabGraph.Text = "Gráfico";
            this.tabGraph.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.barrasUpDown);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(929, 451);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(128, 79);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Painel";
            // 
            // barrasUpDown
            // 
            this.barrasUpDown.Location = new System.Drawing.Point(51, 19);
            this.barrasUpDown.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.barrasUpDown.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.barrasUpDown.Name = "barrasUpDown";
            this.barrasUpDown.Size = new System.Drawing.Size(49, 20);
            this.barrasUpDown.TabIndex = 4;
            this.barrasUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.barrasUpDown.Value = new decimal(new int[] {
            500,
            0,
            0,
            0});
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(25, 45);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Filtrar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Barras:";
            // 
            // cGraficos
            // 
            chartArea1.Name = "ChartArea1";
            this.cGraficos.ChartAreas.Add(chartArea1);
            this.cGraficos.Dock = System.Windows.Forms.DockStyle.Fill;
            legend1.Name = "Legend1";
            this.cGraficos.Legends.Add(legend1);
            this.cGraficos.Location = new System.Drawing.Point(3, 3);
            this.cGraficos.Name = "cGraficos";
            this.cGraficos.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Excel;
            this.cGraficos.Size = new System.Drawing.Size(1054, 532);
            this.cGraficos.TabIndex = 0;
            this.cGraficos.Text = "chart1";
            // 
            // tabHisto
            // 
            this.tabHisto.Controls.Add(this.cHistograma);
            this.tabHisto.Location = new System.Drawing.Point(4, 22);
            this.tabHisto.Name = "tabHisto";
            this.tabHisto.Padding = new System.Windows.Forms.Padding(3);
            this.tabHisto.Size = new System.Drawing.Size(1060, 538);
            this.tabHisto.TabIndex = 1;
            this.tabHisto.Text = "Histograma";
            this.tabHisto.UseVisualStyleBackColor = true;
            // 
            // cHistograma
            // 
            this.cHistograma.BorderSkin.BackColor = System.Drawing.Color.LightGray;
            chartArea2.AxisX.ScaleBreakStyle.LineColor = System.Drawing.Color.DarkGray;
            chartArea2.AxisX.ScaleBreakStyle.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.DashDot;
            chartArea2.AxisY.ScaleBreakStyle.LineColor = System.Drawing.Color.DarkGray;
            chartArea2.AxisY.ScaleBreakStyle.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.DashDot;
            chartArea2.Name = "ChartArea1";
            this.cHistograma.ChartAreas.Add(chartArea2);
            this.cHistograma.Dock = System.Windows.Forms.DockStyle.Fill;
            legend2.Name = "Legend1";
            this.cHistograma.Legends.Add(legend2);
            this.cHistograma.Location = new System.Drawing.Point(3, 3);
            this.cHistograma.Name = "cHistograma";
            this.cHistograma.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Excel;
            this.cHistograma.PaletteCustomColors = new System.Drawing.Color[] {
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))))};
            series1.ChartArea = "ChartArea1";
            series1.IsValueShownAsLabel = true;
            series1.LabelBorderColor = System.Drawing.Color.Transparent;
            series1.Legend = "Legend1";
            series1.MarkerBorderColor = System.Drawing.Color.Black;
            series1.MarkerColor = System.Drawing.Color.Crimson;
            series1.Name = "Frequencia";
            this.cHistograma.Series.Add(series1);
            this.cHistograma.Size = new System.Drawing.Size(1054, 532);
            this.cHistograma.TabIndex = 0;
            this.cHistograma.Text = "chart2";
            // 
            // formIndicador
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1068, 564);
            this.Controls.Add(this.tabControl);
            this.Name = "formIndicador";
            this.Text = "Topos e Vales";
            this.Load += new System.EventHandler(this.formIndicador_Load);
            this.tabControl.ResumeLayout(false);
            this.tabGraph.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.barrasUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cGraficos)).EndInit();
            this.tabHisto.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cHistograma)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public TabControl tabControl;
        public TabPage tabGraph;
        public TabPage tabHisto;
        public TabPage tabSummary;
        public Chart cHistograma;
        public Chart cGraficos;
        private GroupBox groupBox1;
        private NumericUpDown barrasUpDown;
        private Button button1;
        private Label label1;
    }
}

