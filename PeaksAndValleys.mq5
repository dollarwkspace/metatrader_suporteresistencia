//+------------------------------------------------------------------+
//|                                              PeaksAndValleys.mq5 |
//|                                  Copyright 2021, MetaQuotes Ltd. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2021, MetaQuotes Ltd."
#property link      "https://www.mql5.com"
#property version   "1.007"

#import "IndTools.dll"

#include <Math\Stat\Stat.mqh>
#include <lib_cisnewbar.mqh>
#include <Math\Stat\Normal.mqh>
#include <Math\Stat\Math.mqh>
#include <Arrays\ArrayDouble.mqh>
#include <CamiloMacroDefinition.mqh>

#property strict

#property indicator_chart_window
#property indicator_buffers 2
#property indicator_plots   2
//--- plot Maxima
#property indicator_label1  "Maxima"
#property indicator_type1   DRAW_ARROW
#property indicator_color1  clrBlue
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1
//--- plot Minima
#property indicator_label2  "Minima"
#property indicator_type2   DRAW_ARROW
#property indicator_color2  clrRed
#property indicator_style2  STYLE_SOLID
#property indicator_width2  1

//--- input parameters
input bool    ShowArrows=true;
input bool    RealTime=true;
input bool    ShowStats= true;
input int     ArrowsToBeUsedInStats = 20;
input int     barrasHistograma = 3;

//--- indicator buffers
double         MaximaBuffer[];
double         MinimaBuffer[];
bool           BufferInitialized = false;

//--- Variable definition

int historyTotalDeBarras=0;
double _x[], _y[], _x2[], _y2[], _step;
double mediaUp, mediaDown, dispersaoUp, dispersaoDown, obliquidadeUp, obliquidadeDown, curtoseUp, curtoseDown, medianaUp, medianaDown, SdUp, SdDown, AvgDevUp, AvgDevDown;
string msg;

ChartTypes tipoGrafico = point;

//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
  {

//--- indicator buffers mapping
   PlotIndexSetInteger(0,PLOT_DRAW_TYPE,DRAW_ARROW);
   PlotIndexSetInteger(0,PLOT_ARROW,233);
   PlotIndexSetInteger(0,PLOT_ARROW_SHIFT,-10);
   PlotIndexSetDouble(0,PLOT_EMPTY_VALUE,0);
   SetIndexBuffer(0,MaximaBuffer,INDICATOR_DATA);
   ArraySetAsSeries(MaximaBuffer,true);

   PlotIndexSetInteger(1,PLOT_DRAW_TYPE,DRAW_ARROW);
   PlotIndexSetInteger(1,PLOT_ARROW,234);
   PlotIndexSetInteger(1,PLOT_ARROW_SHIFT,10);
   PlotIndexSetDouble(1,PLOT_EMPTY_VALUE,0);
   SetIndexBuffer(1,MinimaBuffer,INDICATOR_DATA);
   ArraySetAsSeries(MinimaBuffer,true);

   ArrayInitialize(MaximaBuffer,0);
   ArrayInitialize(MinimaBuffer,0);
   Comment("");
   IndicatorSetString(INDICATOR_SHORTNAME,"PeaksAndValleys");
   IndicatorSetInteger(INDICATOR_DIGITS,2);

//---
   CSharpTools::ShowForm("formIndicador");
   return(INIT_SUCCEEDED);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
   Print(__FUNCTION__," called! (",INDICATOR_SHORTNAME," Indicador)");
   Print(__FUNCTION__," _UninitReason = ",getUninitReasonText(_UninitReason));
   ChartRedraw(ChartID());
  }


//+------------------------------------------------------------------+
//|Draw Arrows()                                                      |
//+------------------------------------------------------------------+
void DrawArrows()
  {
   PlotIndexSetInteger(0,PLOT_DRAW_TYPE,DRAW_ARROW);
   PlotIndexSetInteger(0,PLOT_ARROW,233);
   PlotIndexSetInteger(0,PLOT_ARROW_SHIFT,-10);
   PlotIndexSetDouble(0,PLOT_EMPTY_VALUE,0);

   PlotIndexSetInteger(1,PLOT_DRAW_TYPE,DRAW_ARROW);
   PlotIndexSetInteger(1,PLOT_ARROW,234);
   PlotIndexSetInteger(1,PLOT_ARROW_SHIFT,10);
   PlotIndexSetDouble(1,PLOT_EMPTY_VALUE,0);
  }

//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
//---
   historyTotalDeBarras = rates_total;
   ArraySetAsSeries(time,true);
   ArraySetAsSeries(open,true);
   ArraySetAsSeries(high,true);
   ArraySetAsSeries(low,true);
   ArraySetAsSeries(close,true);
   ArraySetAsSeries(tick_volume,true);
   ArraySetAsSeries(volume,true);
   ArraySetAsSeries(spread,true);


   if(!BufferInitialized)
     {
      Print("Buffer not initialized!");
      int i=0;
      //while(i<ArraySize(time)-3)
      while(i<1000)
        {
         if(high[i+1]>=high[i+2] && high[i+1]>=high[i])
           {
            MaximaBuffer[i+1]=high[i+1];
           }
         else
           {
            MaximaBuffer[i+1]=0;
           }
         if(low[i+1]<=low[i+2] && low[i+1]<=low[i])
           {
            MinimaBuffer[i+1]=low[i+1];
           }
         else
           {
            MinimaBuffer[i+1]=0;
           }
         i++;
        }
      BufferInitialized = true;
      double _open[1000],_high[1000],_close[1000],_low[1000], maximos[1000], minimos[1000];
      string _time[1000];

      for(int _i=0; _i<1000; _i++)
        {
         _open[_i]=open[_i];
         _high[_i]=high[_i];
         _low[_i]=low[_i];
         _close[_i]=close[_i];
         maximos[_i]=MaximaBuffer[_i];
         minimos[_i]=MinimaBuffer[_i];
         _time[_i]=TimeToString(time[i]);
        }

      Print("OHLC copiados!");
      CSharpTools::LoadPrecos(_time, _open, _high, _low, _close);
      Print(CSharpTools::GetMessage());

      Print("Topos carregados:" + CSharpTools::LoadTopos(maximos, point));
      Print(CSharpTools::GetMessage());

      Print("Vales carregados:"+CSharpTools::LoadVales(minimos, point));
      Print(CSharpTools::GetMessage());

      CSharpTools::EnableGraphs();
      Print(CSharpTools::GetMessage());

     }

//if(rates_total<=prev_calculated)
//   return(rates_total);

   if(high[1]>=high[2] && high[1]>=close[0])
      MaximaBuffer[1]=high[1];
   else
      MaximaBuffer[1]=0;
   if(low[1]<=low[2] && low[1]<=close[0])
      MinimaBuffer[1]=low[1];
   else
      MinimaBuffer[1]=0;
   if(high[2]>=high[3] && high[2]>=high[1])
      MaximaBuffer[2]=high[2];
   else
      MaximaBuffer[2]=0;
   if(low[2]<=low[3] && low[2]<=low[1])
      MinimaBuffer[2]=low[2];
   else
      MinimaBuffer[2]=0;

   if(RealTime)
     {
      if(close[0]>=high[1])
         MaximaBuffer[0]=close[0];
      else
         MaximaBuffer[0]=0;
      if(close[0]<=low[1])
         MinimaBuffer[0]=close[0];
      else
         MinimaBuffer[0]=0;
     }

   if(rates_total<=prev_calculated)
      return(rates_total);

   if(ShowStats)
     {
      PrintStats();
     }

//--- return value of prev_calculated for next call
   return(rates_total);
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void PrintStats()
  {
   uint arrows = ArrowsToBeUsedInStats;
   uint counterUp=0;
   uint counterDown=0;
   double arrowsUp[], arrowsDown[];
   ArrayResize(arrowsUp,arrows);
   ArrayResize(arrowsDown,arrows);

   int i=0;
   while(i<historyTotalDeBarras && (counterUp<arrows || counterDown<arrows))
     {
      if(MaximaBuffer[i]!=0 && counterUp<arrows)
        {
         arrowsUp[counterUp]=MaximaBuffer[i];
         counterUp++;
        }
      if(MinimaBuffer[i]!=0 && counterDown<arrows)
        {
         arrowsDown[counterDown]=MinimaBuffer[i];
         counterDown++;
        }
      i++;
     }

   MathMoments(arrowsUp,mediaUp,dispersaoUp,obliquidadeUp,curtoseUp);
   MathMoments(arrowsDown,mediaDown,dispersaoDown,obliquidadeDown,curtoseDown);
   medianaUp = MathMedian(arrowsUp);
   medianaDown = MathMedian(arrowsDown);
   SdUp = MathStandardDeviation(arrowsUp);
   SdDown = MathStandardDeviation(arrowsDown);
   AvgDevUp = MathAverageDeviation(arrowsUp);
   AvgDevDown = MathStandardDeviation(arrowsDown);

   ArrayFree(arrowsUp);
   ArrayFree(arrowsDown);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void RefreshGraph()
  {
   PrintStats();
   ChartRedraw(ChartID());
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void CreateGraph(double &arr[], double media, double sd, double &interval[], double &freq[], double &resultX[], double &resultY[], bool update = false)
  {
   int min = ArrayMinimum(arr,0,WHOLE_ARRAY), max = ArrayMaximum(arr,0,WHOLE_ARRAY);
   int ncells=barrasHistograma;

   CalculateHistogramArray(arr,interval,freq,arr[max],arr[min],ncells);

   if(ArraySize(interval)<=0 || ArraySize(freq)<=0)
      return;

   GetMaxMinStepValues(arr[max],arr[min],_step);
   _step=MathMin(_step,(arr[max]-arr[min])/ncells);
   MathSequence(arr[min],arr[max],_step,resultX);
   MathProbabilityDensityNormal(resultX,media,sd,false,resultY);

//--- dimensionamos

   double theor_max=resultY[ArrayMaximum(resultY,0,WHOLE_ARRAY)];
   double sample_max=freq[ArrayMaximum(freq,0,WHOLE_ARRAY)];
   double k=sample_max/theor_max;
   for(int i=0; i<ncells; i++)
      freq[i]/=k;

  }

//+------------------------------------------------------------------+
//| ChartEvent function                                              |
//+------------------------------------------------------------------+
void OnChartEvent(const int id,
                  const long &lparam,
                  const double &dparam,
                  const string &sparam)
  {
//---
   if(id==CHARTEVENT_KEYDOWN)
     {
      switch(lparam)
        {
         case KEY_NUMLOCK_LEFT:
            Print("O KEY_NUMLOCK_LEFT foi pressionado");
            break;
         case KEY_LEFT:
            Print("O KEY_LEFT foi pressionado");
            break;
         case KEY_NUMLOCK_UP:
            Print("O KEY_NUMLOCK_UP foi pressionado");
            break;
         case KEY_UP:
            Print("O KEY_UP foi pressionado");
            break;
         case KEY_NUMLOCK_RIGHT:
            Print("O KEY_NUMLOCK_RIGHT foi pressionado");
            break;
         case KEY_RIGHT:
            Print("O KEY_RIGHT foi pressionado");
            break;
         case KEY_NUMLOCK_DOWN:
            Print("O KEY_NUMLOCK_DOWN foi pressionado");
            break;
         case KEY_DOWN:
            Print("O KEY_DOWN foi pressionado");
            RefreshGraph();
            ChartRedraw();
            break;

         case KEY_NUMPAD_5:
            Print("O KEY_NUMPAD_5 foi pressionado");
            break;
         case KEY_NUMLOCK_5:
            Print("O KEY_NUMLOCK_5 foi pressionado");
            break;
         default:
            Print("Algumas teclas não listadas foram pressionadas");
        }
     }

   if(id==CHARTEVENT_CHART_CHANGE)
     {
      //Print(__FUNCTION__," Show:",Show);
      if(ShowArrows)
         DrawArrows();
      else
        {
         PlotIndexSetInteger(0,PLOT_DRAW_TYPE,DRAW_NONE);
         PlotIndexSetInteger(1,PLOT_DRAW_TYPE,DRAW_NONE);
        }
      ChartRedraw(ChartID());
     }
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string getUninitReasonText(int reasonCode)
  {
   string text="";
//---
   switch(reasonCode)
     {
      case REASON_ACCOUNT:
         text="Account was changed";
         break;
      case REASON_CHARTCHANGE:
         text="Symbol or timeframe was changed";
         break;
      case REASON_CHARTCLOSE:
         text="Chart was closed";
         break;
      case REASON_PARAMETERS:
         text="Input-parameter was changed";
         break;
      case REASON_RECOMPILE:
         text="Program "+__FILE__+" was recompiled";
         break;
      case REASON_REMOVE:
         text="Program "+__FILE__+" was removed from chart";
         break;
      case REASON_TEMPLATE:
         text="New template was applied to chart";
         break;
      default:
         text="Another reason";
     }
//---
   return text;
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|  Calculate frequencies for data set                              |
//+------------------------------------------------------------------+
bool CalculateHistogramArray(const double &data[],double &intervals[],double &frequency[],
                             double &maxv,double &minv,const int cells=10)
  {
   if(cells<=1)
      return (false);
   int size=ArraySize(data);
   if(size<cells)
      return (false);
   minv=data[ArrayMinimum(data)];
   maxv=data[ArrayMaximum(data)];
   double range=maxv-minv;
   double width=range/cells;
   if(width==0)
      return false;
   ArrayResize(intervals,cells);
   ArrayResize(frequency,cells);
//--- definimos os centros dos intervalos
   for(int i=0; i<cells; i++)
     {
      intervals[i]=minv+(i+0.5)*width;
      frequency[i]=0;
     }
//--- preenchemos a frequência de aparecimento no intervalo
   for(int i=0; i<size; i++)
     {
      int ind=int((data[i]-minv)/width);
      if(ind>=cells)
         ind=cells-1;
      frequency[ind]++;
     }
   return (true);
  }
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|  Calculates values for sequence generation                       |
//+------------------------------------------------------------------+
void GetMaxMinStepValues(double &maxv,double &minv,double &stepv)
  {
//--- calculamos a magnitude da sequência para obter a precisão da normalização
   double range=MathAbs(maxv-minv);
   int degree=(int)MathRound(MathLog10(range));
//--- normalizamos os valores máx. e mín. com a precisão
   maxv=NormalizeDouble(maxv,degree);
   minv=NormalizeDouble(minv,degree);
//--- definimos o passo de geração da mesma maneira a partir da precisão definida
   stepv=NormalizeDouble(MathPow(10,-degree),degree);
   if((maxv-minv)/stepv<10)
      stepv/=10.;
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
